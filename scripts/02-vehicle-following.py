
#!/usr/bin/env python3
#
# Copyright (c) 2019-2020 LG Electronics, Inc.
#
# This software contains code licensed as described in LICENSE.
#

LANE_WIDTH = 4.0

import time
from environs import Env
from lgsvl.geometry import Vector
import lgsvl
from lgsvl import DriveWaypoint, NPCControl

env = Env()

sim = lgsvl.Simulator(env.str("LGSVL__SIMULATOR_HOST", "127.0.0.1"), env.int("LGSVL__SIMULATOR_PORT", 8181))
if sim.current_scene == "SanFrancisco":
    sim.reset()
else:
    sim.load("SanFrancisco")

spawns = sim.get_spawn()

state = lgsvl.AgentState()
state.transform = spawns[0]

forward = lgsvl.utils.transform_to_forward(spawns[0])
right = lgsvl.utils.transform_to_right(spawns[0])

state.transform.position = Vector(0,10,100) + LANE_WIDTH * right

ego = sim.add_agent("2e9095fa-c9b9-4f3f-8d7d-65fa2bb03921", lgsvl.AgentType.EGO, state)

state = lgsvl.AgentState()

# 10 meters ahead, on left lane
state.transform.position = spawns[0].position + 35 * forward
state.transform.rotation.y = spawns[0].rotation.y


npc0 = sim.add_agent("Hatchback", lgsvl.AgentType.NPC, state)
npc0.follow_closest_lane(True, 8, isLaneChange = False)


state = lgsvl.AgentState()

# 10 meters ahead, on left lane
state.transform.position = spawns[0].position - 35 * forward + LANE_WIDTH * right
state.transform.rotation.y = spawns[0].rotation.y


npc1 = sim.add_agent("Hatchback", lgsvl.AgentType.NPC, state)
npc1.follow_closest_lane(True, 8, isLaneChange = False)

sim.run()
