
#!/usr/bin/env python3
#
# Copyright (c) 2019-2020 LG Electronics, Inc.
#
# This software contains code licensed as described in LICENSE.
#

LANE_WIDTH = 4.0

import time
from environs import Env
from lgsvl.geometry import Vector
import lgsvl
from lgsvl import DriveWaypoint, NPCControl

env = Env()

sim = lgsvl.Simulator(env.str("LGSVL__SIMULATOR_HOST", "127.0.0.1"), env.int("LGSVL__SIMULATOR_PORT", 8181))
if sim.current_scene == "SanFrancisco":
    sim.reset()
else:
    sim.load("SanFrancisco")

spawns = sim.get_spawn()

state = lgsvl.AgentState()
state.transform = spawns[0]

forward = lgsvl.utils.transform_to_forward(spawns[0])
right = lgsvl.utils.transform_to_right(spawns[0])

print("forward: ", forward)
print("right: ", right)

state.transform.position = Vector(0,10,100) + LANE_WIDTH * right

ego = sim.add_agent("2e9095fa-c9b9-4f3f-8d7d-65fa2bb03921", lgsvl.AgentType.EGO, state)

state = lgsvl.AgentState()

# 10 meters ahead, on left lane
state.transform.position = spawns[0].position + 30 * forward - 4.0 * right
state.transform.rotation.y = 180 + spawns[0].rotation.y

wp = [
    DriveWaypoint(spawns[0].position + 25 * forward - 4.0 * right,4, Vector(0,0,0)),
    DriveWaypoint(spawns[0].position + 22 * forward - 3.0 * right,1, Vector(0,-15,0)),
    DriveWaypoint(spawns[0].position + 20 * forward - 2.0 * right,1, Vector(0,-90,0)),
    DriveWaypoint(spawns[0].position + 22 * forward - 1.0 * right, 3, Vector(0,-160,0)),
    DriveWaypoint(spawns[0].position + 25 * forward, 4, Vector(0,180,0)),
    DriveWaypoint(spawns[0].position + 35 * forward, 8, Vector(0,180,0)),
]

def on_waypoint(agent, index):
    if index == len(wp) - 1:
        agent.follow_closest_lane(True, 8, isLaneChange=True)

npc0 = sim.add_agent("Hatchback", lgsvl.AgentType.NPC, state)
# npc0.follow_closest_lane(True, 8, isLaneChange = False)
c = NPCControl()
c.turn_signal_left = True
c.headlights = 1
npc0.apply_control(c)
npc0.follow(wp, loop=False)
npc0.on_waypoint_reached(on_waypoint)

# npc0.change_lane(False)
sim.run()
