
#!/usr/bin/env python3
#
# Copyright (c) 2019-2020 LG Electronics, Inc.
#
# This software contains code licensed as described in LICENSE.
#

LANE_WIDTH = 4.0

import time
from environs import Env
from lgsvl.geometry import Vector
import lgsvl

env = Env()

sim = lgsvl.Simulator(env.str("LGSVL__SIMULATOR_HOST", "127.0.0.1"), env.int("LGSVL__SIMULATOR_PORT", 8181))
if sim.current_scene == "SanFrancisco":
    sim.reset()
else:
    sim.load("SanFrancisco")

spawns = sim.get_spawn()

state = lgsvl.AgentState()
state.transform = spawns[0]

forward = lgsvl.utils.transform_to_forward(spawns[0])
right = lgsvl.utils.transform_to_right(spawns[0])

print("forward: ", forward)
print("right: ", right)

state.transform.position = Vector(0,10,100) + LANE_WIDTH * right

ego = sim.add_agent("2e9095fa-c9b9-4f3f-8d7d-65fa2bb03921", lgsvl.AgentType.EGO, state)

state = lgsvl.AgentState()

# 10 meters ahead, on left lane
state.transform.position = spawns[0].position + 25 * forward + 4.0 * right
state.transform.rotation.y = spawns[0].rotation.y
state.velocity = 10 * forward
npc0 = sim.add_agent("Hatchback", lgsvl.AgentType.NPC, state)
# npc0.follow_closest_lane(True, 8, isLaneChange = False)


state = lgsvl.AgentState()

# 10 meters ahead, on left lane
state.transform.position = spawns[0].position + 15 * forward + 4.0 * right
state.transform.rotation.y = spawns[0].rotation.y
state.velocity = 10 * forward
npc1 = sim.add_agent("Hatchback", lgsvl.AgentType.NPC, state)
# npc1.follow_closest_lane(True, 8, isLaneChange = False)

state = lgsvl.AgentState()

state.transform.position = spawns[0].position + 4.0 * right
state.transform.rotation.y = spawns[0].rotation.y
state.velocity = 10 * forward
npc2 = sim.add_agent("Sedan", lgsvl.AgentType.NPC, state)
# npc2.follow_closest_lane(True, 8, isLaneChange = False)

state = lgsvl.AgentState()

state.transform.position = spawns[0].position - 20 * forward + 4.0 * right
state.transform.rotation.y = spawns[0].rotation.y
state.velocity = 10 * forward
npc3 = sim.add_agent("Sedan", lgsvl.AgentType.NPC, state)
# npc3.follow_closest_lane(True, 8, isLaneChange = False)

state = lgsvl.AgentState()

state.transform.position = spawns[0].position + 20 * forward
state.transform.rotation.y = spawns[0].rotation.y
state.velocity = 15 * forward
npc4 = sim.add_agent("SUV", lgsvl.AgentType.NPC, state)
# npc4.follow_closest_lane(True, 12)

# state = lgsvl.AgentState()
# # 10 meters ahead, on right lane
# state.transform.position = spawns[0].position + 141.5 * forward - 67 * right 
# state.transform.rotation.y = spawns[0].rotation.y - 90

# npc3 = sim.add_agent("SUV", lgsvl.AgentType.NPC, state, lgsvl.Vector(1, 1, 0))


# If the passed bool is False, then the NPC will not moved
# The float passed is the maximum speed the NPC will drive
# 11.1 m/s is ~40 km/h

# comment to make the car not moving
# npc1.follow_closest_lane(True, 11.1)


# 5.6 m/s is ~20 km/h

# comment to make the car not moving
# npc2.follow_closest_lane(True, 5.6)

#Connect to dreamview


print("Bridge connected:", ego.bridge_connected)
#Connect to Apollo bridge
ego.connect_bridge(env.str("LGSVL__AUTOPILOT_0_HOST", lgsvl.wise.SimulatorSettings.bridge_host), env.int("LGSVL__AUTOPILOT_0_PORT", lgsvl.wise.SimulatorSettings.bridge_port))
while not ego.bridge_connected:
    time.sleep(1)
print("Bridge connected:", ego.bridge_connected)

#Connect to dreamview to control Apollo
dv = lgsvl.dreamview.Connection(sim, ego, ip="localhost", port="8888")
dv.set_hd_map("sanfrancisco")
dv.set_vehicle("Lincoln2017MKZ_LGSVL")
modules = [
    "Localization",
    "Transform",
    "Perception",
    "Traffic Light",
    "Planning",
    "Prediction",
    "Camera",
    "Routing",
    "Control"
]

#Set a destination for apollo
state = lgsvl.AgentState()
state.transform.position.x 
destination = state.transform.position + (500 * forward)
dv.setup_apollo(destination.x, destination.z, modules)

sim.run()
