#!/usr/bin/env python3
#
# Copyright (c) 2019-2020 LG Electronics, Inc.
#
# This software contains code licensed as described in LICENSE.
#

LANE_WIDTH = 4.0

from environs import Env
from lgsvl.geometry import Vector
import lgsvl

env = Env()

sim = lgsvl.Simulator(env.str("LGSVL__SIMULATOR_HOST", "127.0.0.1"), env.int("LGSVL__SIMULATOR_PORT", 8181))
if sim.current_scene == "SanFrancisco":
    sim.reset()
else:
    sim.load("SanFrancisco")

spawns = sim.get_spawn()

state = lgsvl.AgentState()
state.transform = spawns[0]

forward = lgsvl.utils.transform_to_forward(spawns[0])
right = lgsvl.utils.transform_to_right(spawns[0])

print("forward: ", forward)
print("right: ", right)

state.transform.position = Vector(0,10,100) + LANE_WIDTH * right

sim.add_agent(env.str("LGSVL__VEHICLE_0", "Lincoln2017MKZ"), lgsvl.AgentType.EGO, state)

state = lgsvl.AgentState()

# 141 meters ahead, on left lane

# 
state.transform.position = spawns[0].position + 141.5 * forward - 66 * right
state.transform.rotation.y = spawns[0].rotation.y - 90

npc1 = sim.add_agent("SUV", lgsvl.AgentType.NPC, state)

state = lgsvl.AgentState()
# 10 meters ahead, on right lane
state.transform.position = spawns[0].position + 141.5 * forward - 72 * right 
state.transform.rotation.y = spawns[0].rotation.y - 90

npc2 = sim.add_agent("Hatchback", lgsvl.AgentType.NPC, state, lgsvl.Vector(1, 1, 0))


state = lgsvl.AgentState()
# 10 meters ahead, on right lane
state.transform.position = spawns[0].position + 141.5 * forward - 90 * right 
state.transform.rotation.y = spawns[0].rotation.y - 90

npc3 = sim.add_agent("SUV", lgsvl.AgentType.NPC, state, lgsvl.Vector(1, 1, 0))

state = lgsvl.AgentState()
# 10 meters ahead, on right lane
state.transform.position = spawns[0].position + 141.5 * forward - 100 * right 
state.transform.rotation.y = spawns[0].rotation.y - 90

npc3 = sim.add_agent("Sedan", lgsvl.AgentType.NPC, state, lgsvl.Vector(1, 1, 0))



# state = lgsvl.AgentState()
# # 10 meters ahead, on right lane
# state.transform.position = spawns[0].position + 141.5 * forward - 67 * right 
# state.transform.rotation.y = spawns[0].rotation.y - 90

# npc3 = sim.add_agent("SUV", lgsvl.AgentType.NPC, state, lgsvl.Vector(1, 1, 0))


# If the passed bool is False, then the NPC will not moved
# The float passed is the maximum speed the NPC will drive
# 11.1 m/s is ~40 km/h

# comment to make the car not moving
# npc1.follow_closest_lane(True, 11.1)


# 5.6 m/s is ~20 km/h

# comment to make the car not moving
# npc2.follow_closest_lane(True, 5.6)

sim.run()
